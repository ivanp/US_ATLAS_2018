\frame{\frametitle{Overview}
  \href{https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/CONFNOTES/ATLAS-CONF-2018-028/}%
       {ATLAS-CONF-2018-028},
  $79.8~\mathrm{fb}^{-1}$ of $\sqrt{s}=13~\mathrm{TeV}$ data:
  \begin{itemize}
    \item Update on the $36.1~\ifb$ paper, \arXiv{1802.04146}.
    \item Fiducial cross section measurement.
    \item Differential cross sections:
      $p_\mathrm{T}(\gamma\gamma)$,
      $|y(\gamma\gamma)|$,
      $p_\mathrm{T}(\text{jet}_1)$,
      $N(\text{b-jets})$.
    \item Production mode cross sections:
      \textit{ggF}, \textit{VBF}, \textit{VH}, \ttH.
    \item Simplified Template Cross Sections (STXS).
    \begin{itemize}
      \item Merged stage1 categories.
    \end{itemize}
    \item Reduced statistical uncertainties and additional differential
      measurements.
  \end{itemize}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\frame{\frametitle{Photon reconstruction and selection}
  \vspace{-10pt}
  \begin{itemize}
    \item EM calorimeter dynamic topological cell clusters
    \item \red{$\abseta < 2.37 \setminus \left(1.37 < \abseta < 1.52\right)$}
    % \item Loose id
    \item 2 highest $\pt$ photons
    \item Diphoton primary vertex id
    \begin{itemize}
      \item Neural-net algorithm {\scriptsize($76\%$ efficient)}
    \end{itemize}
    \item Tight id {\scriptsize($84\%$ to $98\%$ efficient above 25 GeV)}
    \item Track-based isolation requirements
    \begin{itemize}
      \item Suppresses misidentification of jets as photons.
    \end{itemize}
  \item \red{$\pt/\myy > 0.35\ (0.25)$}
  \end{itemize}
  \begin{itemize}
    \item Id based on shower shape variables
    \item Diphoton event trigger has $>98\%$ efficiency for events that pass
      selection.
  \end{itemize}
}

\frame{\frametitle{Jet reconstruction and selection}
  \begin{itemize}
    \item Calorimeter topological clusters, \red{anti-$k_\mathrm{t}$ $R=0.4$}
    \item \red{$\abseta < 4.4$, $\pt > 30$ GeV}
    \begin{itemize}
      \item $\pt > 25$ GeV for $\ttH$ categories
    \end{itemize}
  \item Jet Vertex Tagger (JVT) multivariate discriminant
    \begin{itemize}
      \item Suppresses pile-up jets with $\abseta < 2.5$ and $\pt < 60$ GeV
    \end{itemize}
  \end{itemize}
}

\frame{\frametitle{Pile-up}
  \begin{itemize}
    \item In Run 2, pile-up has been above the LHC design value of
      $\langle\mu\rangle\approx19$.
    \item 2015\plus2016 data: $\langle\mu\rangle\approx24$.
    \item 2017 data: $\langle\mu\rangle\approx38$.
  \end{itemize}
  \begin{center}
    \includegraphics[width=0.6\linewidth]{pileup}
  \end{center}
  \vspace{-15pt}
  \begin{itemize}
    \item Stable purity as a function of pile-up.
  \end{itemize}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\frame{\frametitle{Signal and background models}
  \vspace{-5pt}
  \textbf{Signal model}
  \begin{itemize}
    \item Double-sided Crystal Ball function.
    \begin{itemize}
      \item Gaussian with power-law tails
    \end{itemize}
    \item $N$, $\myy$, 5 more parameters (fit to MC).
    \item $\myy$ fixed to 125.09 GeV for signal extraction.
  \end{itemize}
  \vspace{8pt}
  \textbf{Background model}
  \begin{itemize}
    \item Power law, Bernstein poly (3,4,5), exp of poly (1,2,3).
    \item Spurious signal from MC.
    \begin{itemize}
      \item Systematic unc. due to bkg function choice.
      \item S\plus B model fit to Sherpa $\gamma\gamma$ reweighted to account
        for $\gamma\text{\textit{j}}$ and \textit{jj} contributions.
      \item Max Sig fit for $\myy\in[121,129]$ GeV.
      \item $<10\%$ SM yield, or $<20\%$ expected error,\\
        or within $2\sigma$ of stat. unc. from either condition.
      \item Chose function with fewer parameters.
    \end{itemize}
  \end{itemize}
}

\frame{\frametitle{Signal extraction}
  \begin{itemize}
    \item Unbinned extended likelihood fit:
    {\small
      $$\mathcal{L} = \frac{e^{-\nu}}{n!}\prod_j^n \Big[
        \nu^\text{sig} \mathcal{S}\left(m_{\gamma\gamma}^j;\theta_k\right) +
        \nu^\text{bkg} \mathcal{B}\left(m_{\gamma\gamma}^j\right)
        \Big]$$
    }
    \item Bkg model fit to sidebands: $\myy\in[105,121]\cup[129,160]$ GeV.
    \item S\plus B model fit to $\myy\in[105,160]$ GeV.
    \item The likelihood is the product of $\mathcal{L}$ for each
      bin multiplied by the product of Gaussian and log-normal constraints
    {\small $$\prod_k G\left(\theta_k;0,1\right)$$}
    \item 45 nuisance parameters $\theta_k$ (9 PER, 35 PES, 1 $m_H$).
  \end{itemize}
}

\frame{\frametitle{Signal extraction}
  \vspace{-9.5pt}
  \begin{center}
    \includegraphics[width=0.75\linewidth]{fig_12}
  \end{center}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\frame{\frametitle{Fiducial cross section}
  Measured inclusive fiducial $\Hyy$ cross section:
  $$\tcboxmath{\bm{
    \sigma_\mathrm{fid} = 60.4
      \pm 6.1~\mathrm{(stat.)}
      \pm 6.0~\mathrm{(exp.)}
      \pm 0.3~\mathrm{(theor.)}
      ~\mathrm{fb}
    }}
  $$
  SM prediction
  {\scriptsize(Powheg NNLOPS, N$^\text{3}$LO(QCD)+NLO(EW) norm.)}:
  $$\tcboxmath{\bm{
    \sigma_\mathrm{fid} = 63.5 \pm 3.3 ~\mathrm{fb}
    }}
  $$
  \begin{itemize}
    \item Minimal model dependence (no extrapolation outside detector).
    \item Measurements corrected for detector effects to particle level using
      bin-by-bin correction factors.
  \end{itemize}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\PowhegMC}{
  \colorbox[HTML]{6699FF}{Default MC}: Powheg NNLOPS,
    N$^\text{3}$LO(QCD)+NLO(EW) norm.}

\frame{\frametitle{Differential cross sections:
    $\bm{\gamma\gamma}$ $\bm{p_\mathrm{T}}$}
  \begin{itemize}
    \footnotesize
    \item \PowhegMC
    \item \colorbox[HTML]{CC00CC}{NNLOJET+SCET}:
      NNLO+N$^\text{3}$LL resummation.
  \end{itemize}
  \begin{center}
    \includegraphics[height=0.7\paperheight]{fig_13a}
  \end{center}
  \PlaceText{0.79}{0.25}{17 bins}
  \PlaceText[align=right,font=\scriptsize]{0.81}{0.3}{
    0 \\ 5 \\ 10 \\ 15 \\ 20 \\ 25 \\ 30 \\ 35 \\ 45 \\ 60 \\ 80 \\ 100 \\
    120 \\ 140 \\ 170 \\ 200 \\ 250 \\ 350
  }
}

\frame{\frametitle{Differential cross sections: jet $\bm{p_\mathrm{T}}$}
  \begin{itemize}
    \footnotesize
    \item \PowhegMC
    \item \colorbox[HTML]{FF0000}{NNLOJET}: NNLO accuracy.
    \item \colorbox[HTML]{FFCC33}{SCETlib}: NNLO+NNLL accuracy.
  \end{itemize}
  \begin{center}
    \includegraphics[height=0.7\paperheight]{fig_13c}
  \end{center}
  \PlaceText{0.79}{0.295}{5 bins}
  \PlaceText[align=right,font=\scriptsize]{0.795}{0.345}{
    no jets \\
    30 \\ 55 \\ 75 \\ 120 \\ 350
  }
}

\frame{\frametitle{Differential cross sections: $\bm{\gamma\gamma}$ rapidity}
  \begin{itemize}
    \footnotesize
    \item \PowhegMC
    \item \colorbox[HTML]{BBBB00}{SCETlib+MCFM8}: NNLO+NNLL accuracy.
  \end{itemize}
  \begin{center}
    \includegraphics[height=0.7\paperheight]{fig_13b}
  \end{center}
  \PlaceText{0.79}{0.25}{9 bins}
  \PlaceText[align=right,font=\scriptsize]{0.81}{0.3}{
    0.00 \\ 0.15 \\ 0.30 \\ 0.45 \\ 0.60 \\ 0.75 \\ 0.90 \\ 1.20 \\ 1.60 \\ 2.40
  }
}

\frame{\frametitle{Differential cross sections: N b-jets}
  \begin{itemize}
    \footnotesize
    \item \PowhegMC
    \item \textbf{New} measurement in $\Hyy$ channel.
    \item Same selection
      + lepton veto (suppress $\ttH$) \& $\geq1$ central jet.
  \end{itemize}
  \begin{center}
    \includegraphics[height=0.7\paperheight]{fig_13d}
  \end{center}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\frame{\frametitle{Measured production mode cross sections}
  \begin{center}
    \includegraphics[width=0.8\linewidth]{xsec-mode}
  \end{center}
  \vspace{-15pt}
  \begin{itemize}
    \scriptsize
    \item Top signal strength different but compatible with dedicated ATLAS
      \ttH result \arXiv{1806.00425}:
      $\sigma_\text{\ttH}/\sigma_\text{\ttH}^\text{SM} = 1.39_{-0.42}^{+0.48}$.
  \end{itemize}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\frame{\frametitle{Simplified Template Cross Sections}
  \begin{itemize}
    \item Provide more finely-grained measurements than production mode
      measurements (but less than differential measurements).
    \item Reduce theoretical uncertainties folded into measurements.
    \begin{itemize}
      \item Use simplified volumes compared to fiducial measurement.
    \end{itemize}
    \item Isolate possible BSM effects in BSM-sensitive categories.
    \item Allow for combination of various Higgs decay modes.
    \begin{itemize}
      \item $\Hyy$, $H\rightarrow ZZ$, $H\rightarrow b\bar{b}$, ...
    \end{itemize}
    \item Granularity increases in stages:
    \begin{itemize}
      \item Stage 0: Higgs production modes + $\left|y_H\right|<2.5$
      \item Stage 1: 31 STXS categories, merged to down to 9 for this analysis.
    \end{itemize}
    \item Details in handbook: \arXiv{1610.07922}.
  \end{itemize}
}

\frame{\frametitle{Merged STXS categories}
  \begin{changemargin}{-15pt}{-15pt}
  \begin{center}
    \begin{tikzpicture}
      \definecolor{boxcolor}{HTML}{00AA00}
      \tikzstyle{box}=[thick,draw=boxcolor]
      \draw[box] (-4.17,0.63) rectangle (-2.28,1.12);
      \draw[box] (-1.56,0.43) rectangle (-0.04,-0.07);
      \draw[box] (-1.56,-0.27) rectangle (-0.04,-0.77);
      \draw[box] (-1.56,-0.96) rectangle (-0.04,-1.46);
      \draw[box] (0.03,1.13) rectangle (4.15,-1.46);
      \draw[box] (-1.56,-1.66) rectangle (2.07,-2.95);
      \draw[box] (-7.75,-2.45) rectangle (-1.73,-5.18);
      \tikzstyle{whiteout}=[fill=white,draw=white]
      \draw[whiteout] (1.47,-1.72) rectangle (1.99,-1.52);
      \draw[whiteout] (-0.6,-1.72) rectangle (-0.1,-1.52);
      \node {\includegraphics[width=234pt]{stxs_ggF}};
      \node at (-3.7,-3.3) {\includegraphics[width=225pt]{stxs_VBF}};
      \node[right] at (0.6,1.85) {\scriptsize
        $+\Big[gg\rightarrow Z(qq)H\Big]
         +\Big[b\bar{b}H\Big]$
      };
      \node[right] at (-4.12,-1.77) {\scriptsize VBF};
      \node[left] at (-4.35,-1.78) {\scriptsize
        $\Big[qq\rightarrow Z(qq)H\Big]+
         \Big[WH\Big]+$
      };
    \end{tikzpicture}
  \end{center}
  \end{changemargin}
  {\scriptsize Figures from \arXiv{1610.07922}.}
}

\frame{\frametitle{Measured STXS}
  \begin{center}
    \includegraphics[width=0.8\linewidth]{fig_10}
  \end{center}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\frame{\frametitle{High $\bm{p_\mathrm{T}}$ events}
  \begin{itemize}
    \item $\sim 17$ signal events with $\pt^\yy > 350$ GeV.
    \item Approximately 1:1 signal to background ratio at high-$\pt$.
    \item Could identify events with high probability of being Higgs events.
  \end{itemize}
  \begin{center}
    \includegraphics[width=0.8\linewidth]{overflow}
  \end{center}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\frame{\frametitle{Conclusion}
  \href{https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/CONFNOTES/ATLAS-CONF-2018-028/}%
       {ATLAS-CONF-2018-028},
  $79.8~\mathrm{fb}^{-1}$ of $\sqrt{s}=13~\mathrm{TeV}$ data.
  \begin{itemize}
    \item Improved precision.
    \item Additional differential measurements.
  \end{itemize}
  \vspace{8pt}
  Outlook for Run 2:
  \begin{itemize}
    \item Paper with full 2015 -- 2018 dataset, $\sim 150~\ifb$.
    \item Work on decreasing systematic uncertainties.
    \item Statistical uncertainties will decrease.
    \item Finer bins for all differential measurements.
    \item Better statistics for high-$\pt$ events.
    \begin{itemize}
      \item $\sim 30$ events with $\pt^\yy>350$ GeV with $150~\ifb$.
    \end{itemize}
  \end{itemize}
}

